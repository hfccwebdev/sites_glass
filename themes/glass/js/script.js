/* get session global variable */
var color = localStorage.getItem('color');

jQuery(document).ready(function($) {

  /* Loop through elements and assign classes */

  var allColorsText = 'orange-text green-text blue-text black-text brown-text yellow-text red-text gray-text purple-text';
  var allColorsHoverText = 'orange-text-hover green-text-hover blue-text-hover black-text-hover brown-text-hover yellow-text-hover red-text-hover gray-text-hover purple-text-hover';
  var allColorsBg = 'orange-bg green-bg blue-bg black-bg brown-bg yellow-bg red-bg gray-bg purple-bg';

  function switchColor($color) {
    $('a').each(function(){
      $(this).removeClass(allColorsText).addClass(color + '-text');
    });
    $('.colorable').each(function(){
      $(this).removeClass(allColorsBg).addClass(color + '-bg');
    });
    $('.colorable-text').each(function(){
      $(this).removeClass(allColorsText).addClass(color + '-text');
    });
    $('.menu li a').each(function(){
      $(this).removeClass(allColorsText + ' ' + allColorsHoverText).addClass(color + '-text-hover');
    });
    $('.menu li a.active').each(function(){
      $(this).removeClass(allColorsText).addClass(color + '-text');
    });
    $('#main-menu li a').each(function(){
      $(this).removeClass(allColorsText + ' ' + allColorsHoverText).addClass(color + '-text-hover');
    });
    $('#main-menu li a.active').each(function(){
      $(this).removeClass(allColorsText).addClass(color + '-text');
    });
    $('#logo img').each(function(){
      $(this).removeClass(allColorsBg).addClass(color + '-bg');
    });
  }

  /* check for stored sessionvariable */

  if (!color) {
    localStorage.setItem('color','red');
    color = localStorage.getItem('color');
  }

  /* run initial switchColor */
  switchColor(color);

  $('#orange').click(function(){
    localStorage.setItem('color','orange');
    color = localStorage.getItem('color');
    switchColor(color);
  });

  $('#green').click(function(){
    localStorage.setItem('color','green');
    color = localStorage.getItem('color');
    switchColor(color);
  });

  $('#blue').click(function(){
    localStorage.setItem('color','blue');
    color = localStorage.getItem('color');
    switchColor(color);
  });

  $('#black').click(function(){
    localStorage.setItem('color','black');
    color = localStorage.getItem('color');
    switchColor(color);
  });

  $('#brown').click(function(){
    localStorage.setItem('color','brown');
    color = localStorage.getItem('color');
    switchColor(color);
  });

  $('#yellow').click(function(){
    localStorage.setItem('color','yellow');
    color = localStorage.getItem('color');
    switchColor(color);
  });

  $('#red').click(function(){
    localStorage.setItem('color','red');
    color = localStorage.getItem('color');
    switchColor(color);
  });

  $('#gray').click(function(){
    localStorage.setItem('color','gray');
    color = localStorage.getItem('color');
    switchColor(color);
  });

  $('#purple').click(function(){
    localStorage.setItem('color','purple');
    color = localStorage.getItem('color');
    switchColor(color);
  });

  /* Circle Click Events */
  $('.circle-frame').click(function(){
    $(this).parent().find('.circle-caption').hide();
    $(this).find('.circle-overlay').hide();
    $(this).parent().find('.button-exit').show();
    $(this).find('.circle-reveal').show();
  });

  $('.button-exit').click(function(){
    $(this).parent().parent().find('.circle-reveal').hide();
    $(this).parent().parent().find('.circle-caption').show();
    $(this).hide();
  });

});
