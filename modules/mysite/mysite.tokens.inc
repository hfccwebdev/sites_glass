<?php

/**
 * @file
 * Token callbacks for the mysite module.
 */

/**
 * Implements hook_token_info().
 */
function mysite_token_info() {
  $info = array();

  // Add any new tokens.
  $info['tokens']['node'] = array(
    'field_issue_path' => array(
      'name' => t('Issue Path'),
      'description' => t('A version of field_publication_date suitable for pathauto use.'),
    ),
    'field_article_issue_path' => array(
      'name' => t('Article Issue Path'),
      'description' => t('A version of field_publication_date suitable for pathauto use in articles.'),
    ),
  );

  // Return them.
  return $info;
}

/**
 * Implements hook_tokens().
 */
function mysite_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  if ($type == 'node') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'field_issue_path':
          if (!empty($data['node']->field_publication_date)) {
            $timestamp = reset($data['node']->field_publication_date[LANGUAGE_NONE]);
            $replacements[$original] = format_date($timestamp['value'], 'custom', 'Y/m-d');
          }
          break;

        case 'field_article_issue_path':
          if (!empty($data['node']->field_issue)) {
            $nid = $data['node']->field_issue[LANGUAGE_NONE]['0']['target_id'];
            $issue = node_load($nid);
            $timestamp = reset($issue->field_publication_date[LANGUAGE_NONE]);
            $replacements[$original] = format_date($timestamp['value'], 'custom', 'Y/m-d');
          }
          break;
      }
    }
  }
  return $replacements;
}
